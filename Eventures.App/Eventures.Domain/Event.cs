﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Eventures.Domain
{
    public class Event
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Place { get; set; }
        public DateTime StartDate  { get; set; }
        public DateTime EndDate { get; set; }
        public int TotalTickets { get; set; }
        public decimal PricePerTickket { get; set; }
    }
}
