﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LMS.Data.Data.Entities
{
    public class Ban
    {
        public string Id { get; set; }
        [Required]
        public string Reason { get; set; }
        public DateTime ExpirationDate { get; set; }
        [Required]
        public string UserId { get; set; }
        public User User { get; set; }
    }
}
