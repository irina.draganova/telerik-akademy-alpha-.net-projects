﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace LMS.Data.Data.Entities
{
    public class Book
    {
        public string Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public int Year { get; set; }

        public virtual ICollection<Grade> Grades { get; set; }
        public virtual ICollection<BookAuthor> BookAuthors { get; set; }
        public virtual ICollection<BookSubject> Subjects { get; set; }
        public virtual IEnumerable<Reservation> Reservations { get; set; }

        public string RentalId { get; set; }
        public Rental Rental { get; set; }

        public string StatusId { get; set; }
        public Status Status { get; set; }
    }
}
