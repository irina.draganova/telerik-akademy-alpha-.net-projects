﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Data.Data.Entities
{
    public class BookAuthor
    {
        public string AuthorId { get; set; }
        public virtual Author Author { get; set; }

        public string BookId { get; set; }
        public virtual Book Book { get; set; }
    }
}
