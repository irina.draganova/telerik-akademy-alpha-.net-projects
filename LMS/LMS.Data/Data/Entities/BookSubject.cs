﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Data.Data.Entities
{
    public class BookSubject
    {
        public string BookId { get; set; }
        public Book Book { get; set; }

        public string SubjectId { get; set; }
        public Subject Subject { get; set; }
    }
}
