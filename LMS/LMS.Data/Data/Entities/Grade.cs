﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LMS.Data.Data.Entities
{
    public class Grade
    {
        [Required]
        public double Rating { get; set; }
        public string Review { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }

        public string BookId { get; set; }
        public Book Book { get; set; }
    }
}
