﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Data.Data.Entities
{
    public class Notification
    {
        public Notification()
        {

        }
        public string Id { get; set; }
        public string Description { get; set; }
        public DateTime? CreatedOn { get; set; }
        public bool IsRead { get; set; }

        public string UserId { get; set; }
        public User Receiver { get; set; }


    }
}
