﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Data.Data.Entities
{
    public class Package
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public decimal Price { get; set; }
        public int RentalDays { get; set; }
        public int ReservationDays { get; set; }

        public ICollection<User> Users { get; set; }
    }
}
