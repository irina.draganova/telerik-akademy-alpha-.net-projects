﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Data.Data.Entities
{
    public class Rental
    {
        public string Id { get; set; } 
        public DateTime ReservedDate { get; set; }

        public string UserId { get; set; } // na pesho id-to
        public User User { get; set; }

        public string BookId { get; set; } // na vlastelina id-to
        public Book Book { get; set; }
    }
}
