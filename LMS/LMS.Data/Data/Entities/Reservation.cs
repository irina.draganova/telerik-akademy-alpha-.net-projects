﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Data.Data.Entities
{
    public class Reservation
    {
        public DateTime ReservedDate { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }

        public string BookId { get; set; }
        public Book Book { get; set; }
    }
}
