﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LMS.Data.Data.Entities
{
    public class Status
    {
        public string Id { get; set; }
        public string Type { get; set; }

        public ICollection<Book> Books { get; set; }
    }
}