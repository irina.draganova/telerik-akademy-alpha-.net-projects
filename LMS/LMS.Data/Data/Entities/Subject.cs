﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Data.Data.Entities
{
    public class Subject
    {
        public string Id { get; set; }
        public string Type { get; set; }

        public ICollection<BookSubject> Books { get; set; }
    }
}
