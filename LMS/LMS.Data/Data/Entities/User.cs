﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Data.Data.Entities
{
    public class User : IdentityUser
    {
        public ICollection<Book> Books { get; set; }
        public ICollection<Grade> Grades { get; set; }
        public ICollection<Notification> Notifications { get; set; }
        public ICollection<Reservation> ReservedBooks { get; set; }
        public ICollection<Rental> Rentals { get; set; }

        public string PackageId { get; set; } 
        public Package Package { get; set; }

        public string BanId { get; set; }
        public Ban Ban { get; set; }
    }
}
