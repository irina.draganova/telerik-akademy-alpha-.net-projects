﻿using LMS.Data.Data.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.Data
{
    public class LMSDbContext : IdentityDbContext<User>
    {
        private List<Status> statusSeeder = new List<Status>() {
            new Status() { Id="a", Type = "Available" },
            new Status() { Id="b", Type = "Taken"},
            new Status() { Id="c", Type = "Reserved"},
            new Status() { Id="d", Type = "ForDeletion"}
        };

        private List<Package> packageSeeder = new List<Package>()
        {
            new Package(){Id="e",Price=0,RentalDays=0,ReservationDays=0,Type="Default"}
        };
        //private List<User> userSeeder=new List<User>() {
        //    new User(){Id="a",d}
        //}
        public DbSet<Author> Authors { get; set; }
        public DbSet<Ban> Bans { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<BookAuthor> BookAuthors { get; set; }
        public DbSet<BookSubject> BookSubjects { get; set; }
        public DbSet<Grade> Grades { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Package> Packages { get; set; }
        public DbSet<Rental> Rentals { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Subject> Subjects { get; set; }

        public LMSDbContext(DbContextOptions<LMSDbContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            //seeders
            builder.Entity<Status>()
                .HasData(statusSeeder);

            builder.Entity<Package>()
                .HasData(packageSeeder);


            //Author
            builder.Entity<Author>()
                .Property(a => a.Name)
                .HasMaxLength(30);

            builder.Entity<Author>()
                .HasMany(a => a.BookAuthors)
                .WithOne(a => a.Author);


            //User
            builder.Entity<Ban>()
                .HasOne(u => u.User)
                .WithOne(b => b.Ban)
                .HasForeignKey<Ban>(u => u.UserId);
               //.HasForeignKey("Ban");


            //Book
            builder.Entity<Book>()
                .Property(b => b.Title)
                .HasMaxLength(40);

            builder.Entity<Book>()
                .Property(b => b.StatusId).HasDefaultValue(statusSeeder[0].Id);

            builder.Entity<Book>()
                .Property(b => b.RentalId)
                .IsRequired(false);

            builder.Entity<Book>()
                .Property(b => b.Title)
                .IsRequired(true);

            //Book
            builder.Entity<Book>()
                .HasOne(b => b.Rental)
                .WithOne(r => r.Book)
                .HasForeignKey("Rental");

            builder.Entity<BookAuthor>()
                .HasKey(ba => new { ba.AuthorId, ba.BookId });

            builder.Entity<BookAuthor>()
                .HasOne(ba => ba.Book)
                .WithMany(b => b.BookAuthors);

            builder.Entity<BookAuthor>()
                .HasOne(ba => ba.Author)
                .WithMany(b => b.BookAuthors);

            builder.Entity<BookSubject>()
                .HasKey(bs => new { bs.BookId, bs.SubjectId });

            builder.Entity<Reservation>()
                .HasKey(r => new { r.BookId, r.UserId });

            builder.Entity<Grade>()
                .HasKey(g => new { g.UserId, g.BookId });

            base.OnModelCreating(builder);
        }
    }
}