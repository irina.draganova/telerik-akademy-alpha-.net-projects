using LMS.Data;
using LMS.Data.Data.Entities;
using LMS.ServiceLayer.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace LMS.ServiceLayer.UnitTests.AuthorServiceShould
{
    [TestClass]
    public class AuthorService_Should
    {
        [TestMethod]
        public void GetAuthorsShould()
        {
            var testAuthorName = "enis";
            var testAuthor = new Author()
            {
                Name = testAuthorName,
            };
            var options = TestUtilities.GetOptions(nameof(GetAuthorsShould));


            using (var arrangeContext = new LMSDbContext(options))
            {
                arrangeContext.Authors.Add(testAuthor);
                arrangeContext.SaveChanges();
            }

            using (var assertContext = new LMSDbContext(options))
            {
                Assert.AreEqual(1, assertContext.Authors.Count());
            }
        }
        [TestMethod]
        public async Task GetValidAuthor()
        {
            var testAuthorName = "enis";
            var testAuthor = new Author()
            {
                Name = testAuthorName,
            };
            var options = TestUtilities.GetOptions(nameof(GetValidAuthor));


            using (var arrangeContext = new LMSDbContext(options))
            {
                await arrangeContext.Authors.AddAsync(testAuthor);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new LMSDbContext(options))
            {
                Assert.AreSame(testAuthor.Name, (await assertContext.Authors.FindAsync(testAuthor.Id)).Name);
            }
        }
        [TestMethod]
        public async Task ThrowWhen_GetAuthorNotValid()
        {
            var options = TestUtilities.GetOptions(nameof(ThrowWhen_GetAuthorNotValid));

            using (var assertContext = new LMSDbContext(options))
            {
                var sut = new AuthorService(assertContext);
                await Assert.ThrowsExceptionAsync<ArgumentNullException>(() => sut.GetAuthorAsync("a"));
            }
        }
        [TestMethod]
        public async Task CreateAuthorShould()
        {
            var testAuthorName = "enis";
            var testAuthor = new Author()
            {
                Name = testAuthorName,
            };
            var options = TestUtilities.GetOptions(MethodBase.GetCurrentMethod().Name);

            using (var arrangeContext = new LMSDbContext(options))
            {
                await arrangeContext.AddAsync(testAuthor);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new LMSDbContext(options))
            {
                Assert.IsNotNull(assertContext.Authors.FirstOrDefault(a => a.Id == testAuthor.Id));
            }
        }
        [TestMethod]
        public async Task EditAuthorShould()
        {
            var testAuthorName = "enis";
            var testAuthor = new Author()
            {
                Name = testAuthorName,
            };
            var options = TestUtilities.GetOptions(MethodBase.GetCurrentMethod().Name);

            using (var arrangeContext = new LMSDbContext(options))
            {
                await arrangeContext.AddAsync(testAuthor);
                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new LMSDbContext(options))
            {
                testAuthor.Name = "meni";
                actContext.Update(testAuthor);
                await actContext.SaveChangesAsync();
            }

            using (var assertContext = new LMSDbContext(options))
            {
                Assert.AreEqual("meni", assertContext.Authors.FirstOrDefault(a => a.Id == testAuthor.Id).Name);
            }
        }
        [TestMethod]
        public async Task DeleteAuthorShould()
        {
            var testAuthorName = "enis";
            var testAuthor = new Author()
            {
                Name = testAuthorName,
            };
            var options = TestUtilities.GetOptions(nameof(DeleteAuthorShould));

            using (var arrangeContext = new LMSDbContext(options))
            {
                await arrangeContext.AddAsync(testAuthor);
                await arrangeContext.SaveChangesAsync();
            }

            using (var actContext = new LMSDbContext(options))
            {
                actContext.Authors.Remove(testAuthor);
                await actContext.SaveChangesAsync();
            }

            using (var assertContext = new LMSDbContext(options))
            {
                Assert.AreEqual(0, assertContext.Authors.Count());
            }
        }
    }
}

//DONE
