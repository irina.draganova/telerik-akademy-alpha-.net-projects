using LMS.Data;
using LMS.Data.Data.Entities;
using LMS.ServiceLayer.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.ServiceLayer.UnitTests
{
    [TestClass]
    public class BookServiceShould
    {
        //TODO
        [TestMethod]
        public async Task CreateBookShould()
        {
            var testTitle = "enis";
            var testYear = 2010;
            var testAuthors = new Author()
            {
                Name = "Asen",
            };

            var testBook = new Book()
            {
                Title = testTitle,
                Year = testYear,
            };

            var options = TestUtilities.GetOptions(nameof(BookServiceShould));

            using (var arrangeContext = new LMSDbContext(options))
            {
                await arrangeContext.Books.AddAsync(testBook);
                await arrangeContext.Authors.AddRangeAsync(testAuthors);
                await arrangeContext.BookAuthors.AddAsync(new BookAuthor{BookId=testBook.Id,AuthorId=testAuthors.Id });
                await arrangeContext.SaveChangesAsync();
            }

            var description = $"A book \"{testTitle}\" has been created";
            Mock<INotificationService> mockNotService;

            //mockNotService.Setup(mtS=>mtS.SendNotificationToAdmin(description))
            using (var assertContext = new LMSDbContext(options))
            {
                Assert.AreEqual(1, assertContext.Books.Count());
            }
        }
        [TestMethod]
        public void TestMethod2()
        {
        }
        [TestMethod]
        public void TestMethod3()
        {
        }
        [TestMethod]
        public void TestMethod4()
        {
        }
        [TestMethod]
        public void TestMethod5()
        {
        }
        [TestMethod]
        public void TestMethod6()
        {
        }
        [TestMethod]
        public void TestMethod7()
        {
        }
        [TestMethod]
        public void TestMethod8()
        {
        }
        [TestMethod]
        public void TestMethod9()
        {
        }
        [TestMethod]
        public void TestMethod10()
        {
        }

    }
}
