﻿using LMS.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.ServiceLayer.UnitTests
{
    public static class TestUtilities
    {
        public static DbContextOptions<LMSDbContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<LMSDbContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }
    }
}
