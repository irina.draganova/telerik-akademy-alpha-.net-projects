﻿using LMS.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace LMS.ServiceLayer.UnitTests.UserServiceShould.Factory
{
    public class LMSDbContextInMemoryFactory
    {
        public static LMSDbContext CreateDbContext()
        {
            var options = new DbContextOptionsBuilder<LMSDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var context = new LMSDbContext(options);

            return context;
        }
    }
}
