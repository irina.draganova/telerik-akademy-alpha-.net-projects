﻿using LMS.Data;
using LMS.Data.Data.Entities;
using LMS.ServiceLayer.Services;
using LMS.ServiceLayer.UnitTests.UserServiceShould.Factory;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.ServiceLayer.UnitTests.UserServiceShould
{
    [TestClass]
    public class UserServiceTests
    {
        [TestMethod]
        public async Task GetAllUsers_WithTestData_ShouldRutrnCorrectResults()
        {
            var options = new DbContextOptionsBuilder<LMSDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var context = new LMSDbContext(options);
            SeedTestData(context);

            var userService = new UserService(context);

            var expectedData = GetTestData();
            var actualData = await userService.GetAllUsers();

            Assert.AreEqual(expectedData.Count, actualData.Count);

            foreach (var actualUser in actualData)
            {
                Assert.IsTrue(expectedData.Any(user =>
                actualUser.UserName == user.UserName &&
                actualUser.Email == user.Email));
            }
        }

        [TestMethod]
        public async Task GetAllUsers_WithoutAnyData_ShouldRutrnEmptyList()    
        {
            var options = new DbContextOptionsBuilder<LMSDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var context = new LMSDbContext(options);

            var userService = new UserService(context);

            var actualData = await userService.GetAllUsers();

            Assert.AreEqual(0, actualData.Count);

        }

        [TestMethod]
        public void TestGetUser_WithExistenUserName_ShouldReturnUser()
        {
            var options = new DbContextOptionsBuilder<LMSDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            var context = new LMSDbContext(options);
            SeedTestData(context);

            var userService = new UserService(context);

            string testUserName = "TestUSer1";

            var expectedData = GetTestData().SingleOrDefault(user => user.UserName == testUserName);
            var actualData = userService.GetUsers(testUserName).SingleOrDefault(user => user.UserName == testUserName);

            Assert.AreEqual(expectedData.UserName, actualData.UserName);
        }

        [TestMethod]
        public void TestGetUser_WithNonExistenUserName_ShouldReturnNull()
        {
            var context = LMSDbContextInMemoryFactory.CreateDbContext();
            SeedTestData(context);

            var userService = new UserService(context);

            string testUserName = "TestUSer3";
            var actualData = userService.GetUsers(testUserName).SingleOrDefault(user => user.UserName == testUserName);

            Assert.IsTrue(actualData == null);

        }
        private void SeedTestData(LMSDbContext context)
        {
            context.AddRange(GetTestData());
            context.SaveChanges();
        }

        private List<User> GetTestData()
        {
            return new List<User>
            {
                new User
                {
                    UserName = "TestUSer1",
                    Email = "TestUser1@mail.com",
                },
                new User
                {
                    UserName = "TestUSer2",
                    Email = "TestUser2@mail.com",
                }
            };
        }
    }
}
