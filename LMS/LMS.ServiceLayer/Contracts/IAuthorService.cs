﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LMS.Data.Data.Entities;

namespace LMS.ServiceLayer.Services
{
    public interface IAuthorService
    {
        Task<Author> CreateAuthorAsync(string name);
        Task<Author> GetAuthorAsync(string id);
        Task<IEnumerable<Author>> GetAuthorsAsync();
        Task DeleteAuthor(string id);
        Task<Author> Edit(Author author);
    }
}