﻿using LMS.Data.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LMS.ServiceLayer.Contracts
{
    public interface IBookService
    {
        Task<Book> CreateBook(string title, int year, List<string> authors);
        Task<IReadOnlyCollection<Book>> GetBooks();
        Task<IReadOnlyCollection<Book>> GetUserBooks(string userId);
        Task<Book> GetBook(string id);
        Task<IReadOnlyCollection<Book>> GetReservedBooksByUser(string userId);
        Task<int> GetPagesCount();
        Task<IReadOnlyCollection<Book>> GetCurrentPageBooks(int id);
    }
}
