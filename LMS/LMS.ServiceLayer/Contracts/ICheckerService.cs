﻿using System.Threading.Tasks;

namespace LMS.ServiceLayer.Contracts
{
    public interface ICheckerService
    {
        Task ReturnBookIfDatePassed();
    }
}