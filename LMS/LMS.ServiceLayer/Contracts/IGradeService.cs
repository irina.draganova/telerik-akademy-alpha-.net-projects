﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LMS.Data.Data.Entities;

namespace LMS.ServiceLayer.Contracts
{
    public interface IGradeService
    {
        Task<Grade> Create(string bookId, string userId, double rating, string description);
        Task<bool> CheckIfBookIsRated(string bookId, string userId);
        Task<Grade> GetGrade(string bookId, string userId);
        Task<Grade> Edit(Grade grade);
        Task<IReadOnlyCollection<Grade>> GetGrades(string bookId);
    }
}