﻿using LMS.Data.Data.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.ServiceLayer.Services
{
    public interface INotificationService
    {
        Task<Notification> CreateNotification(string userId, string description);
        Task<ICollection<Notification>> GetNotifications(string receiverId);
        Task<Notification> ReadNotification(string notificationId);
        Task<Notification> SendNotificationToAdmin(string description);
    }
}