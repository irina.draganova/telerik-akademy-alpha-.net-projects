﻿using LMS.Data.Data.Entities;
using System.Threading.Tasks;

namespace LMS.ServiceLayer.Contracts
{
    public interface IRentalManager
    {
        Task<Book> ChechkoutBookAsync(string userId, string bookId);
        Task<Book> ReturnBookAsync(string userId, string bookId);
    }
}