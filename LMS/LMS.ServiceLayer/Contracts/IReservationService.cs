﻿using System.Collections.Generic;
using System.Threading.Tasks;
using LMS.Data.Data.Entities;

namespace LMS.ServiceLayer.Contracts
{
    public interface IReservationService
    {
        Task<Reservation> Create(string userId, string bookId);
        Task RemoveReservation(string userId, string bookId);
        Task<IReadOnlyCollection<Reservation>> GetReservationsForUserAsync(string id);
    }
}