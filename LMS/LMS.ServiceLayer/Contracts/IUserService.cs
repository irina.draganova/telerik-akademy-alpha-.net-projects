﻿using LMS.Data.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LMS.ServiceLayer.Contracts
{
    public interface IUserService
    {
        Task<Ban> CreateBan(string userId, string reason, int days);
        Task DeleteBan(string userId);
        Task<IReadOnlyCollection<User>> GetAllUsers();
        Task<User> FindUserAsync(string userName);
        IReadOnlyCollection<User> GetUsers(string name);
    }
}
