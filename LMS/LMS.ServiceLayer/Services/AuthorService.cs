﻿using LMS.Data;
using LMS.Data.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.ServiceLayer.Services
{
    public class AuthorService : IAuthorService
    {
        private readonly LMSDbContext context;

        public AuthorService(LMSDbContext context)
        {
            this.context = context;
        }
        public async Task<IEnumerable<Author>> GetAuthorsAsync()
        {
            return await this.context.Authors
                .Include(a => a.BookAuthors)
                    .ThenInclude(ba => ba.Book)
                .ToListAsync();
        }
        public async Task<Author> GetAuthorAsync(string id)
        {
            var author = await this.context.Authors
                .Include(a => a.BookAuthors)
                    .ThenInclude(ba => ba.Book)
                .FirstOrDefaultAsync(a => a.Id == id);
            if (author==null)
            {
                throw new ArgumentNullException();
            }

            return author;
        }
        public async Task<Author> CreateAuthorAsync(string name)
        {
            var author = new Author();
            author.Name = name;

            await this.context.AddAsync(author);
            await this.context.SaveChangesAsync();

            return author;
        }
        public async Task<Author> Edit(Author author)
        {
            this.context.Update(author);
            await this.context.SaveChangesAsync();

            return author;
        }
        public async Task DeleteAuthor(string id)
        {
            var author = await context.Authors.FindAsync(id);
            this.context.Authors.Remove(author);
            await context.SaveChangesAsync();
        }
    }
}
