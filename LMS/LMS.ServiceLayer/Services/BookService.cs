﻿using LMS.Data;
using LMS.Data.Data.Entities;
using LMS.ServiceLayer.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.ServiceLayer.Services
{
    public class BookService : IBookService
    {
        private readonly LMSDbContext context;
        private readonly INotificationService notificationService;

        public BookService(LMSDbContext context,
                           INotificationService notificationService)
        {
            this.context = context;
            this.notificationService = notificationService;
        }

        //public async Task<Book> CreateBook(string title, int year, List<string> authors)
        //{
        //    var book = new Book()
        //    {
        //        Title = title,
        //        Year = year
        //    };

        //    this.context.Books.Add(book);
        //    await context.SaveChangesAsync();

        //    foreach (var item in authors)
        //    {
        //        var currentAuthor = await this.context.Authors
        //            .Where(a => a.Name.Equals(item))
        //            .FirstOrDefaultAsync();

        //        var bookauthor = new BookAuthor()
        //        {
        //            BookId = book.Id,
        //            AuthorId = currentAuthor.Id,
        //        };

        //        this.context.BookAuthors.Add(bookauthor);
        //    }
        //    await this.context.SaveChangesAsync();

        //    var turboAdmin = this.context.Users.FirstOrDefault(u => u.UserName == "admin");

        //    context.Notifications.Add(new Notification()
        //    {
        //        Description = $"A book \"{title}\" has been created",
        //        Receiver = turboAdmin,
        //        UserId = turboAdmin.Id,
        //        CreatedOn = DateTime.Now
        //    });

        //    await this.context.SaveChangesAsync();
        //    var description = $"A book \"{title}\" has been created";
        //    await notificationService.SendNotificationToAdmin(description);

        //    return book;
        //}
        public async Task<Book> CreateBook(string title,int year,List<string> authors)
        {
            var book = new Book()
            {
                Title = title,
                Year = year
            };

            this.context.Books.Add(book);
            await context.SaveChangesAsync();

            foreach (var item in authors)
            {
                var currentAuthor = await this.context.Authors
                    .Where(a => a.Name.Equals(item))
                    .FirstOrDefaultAsync();

                var bookauthor = new BookAuthor()
                {
                    AuthorId = currentAuthor.Id,
                    BookId = book.Id
                };

                await this.context.BookAuthors.AddAsync(bookauthor);
            }
            await this.context.SaveChangesAsync();

            var description = $"A book \"{book.Title}\" has been created";
            await notificationService.SendNotificationToAdmin(description);

            return book;
        }
        public async Task<IReadOnlyCollection<Book>> GetBooks()
        {
            var listOfBooks = await context.Books
                .Include(b => b.Status)
                .Include(b => b.BookAuthors)
                    .ThenInclude(b => b.Author)
                .Include(b => b.Grades)
                .ToListAsync();

            return listOfBooks;
        }
        public async Task<IReadOnlyCollection<Book>> GetReservedBooksByUser(string userId)
        {
            var reservations = await this.context.Reservations.Where(r => r.UserId == userId).ToListAsync();
            var listOfBooks = new List<Book>();

            foreach (var item in reservations)
            {
                var book = await GetBook(item.BookId);
                if (book == null)
                { }
                else
                    listOfBooks.Add(book);
            }

            return listOfBooks;
        }
        public async Task<Book> GetBook(string id)
        {
            var book = await context.Books
                .Include(b => b.Status)
                .Include(b => b.Grades)
                .Include(b => b.Rental)
                .Include(b => b.Reservations)
                .Include(b => b.BookAuthors)
                    .ThenInclude(a => a.Author)
                .FirstOrDefaultAsync(b => b.Id == id);

            if (book == null)
            {
                throw new Exception("Book not found");
            }

            return book;
        }
        public async Task<IReadOnlyCollection<Book>> GetUserBooks(string userId)
        {
            var books = await context.Books
                .Include(b => b.Status)
                .Include(b => b.Grades)
                .Include(b => b.Rental)
                .Include(b => b.Reservations)
                .Include(b => b.BookAuthors)
                    .ThenInclude(a => a.Author)
                .Where(b => b.RentalId == userId).ToListAsync();

            return books;
        }
        public async Task<int> GetPagesCount()
        {
            if (this.context.Books.Count() % 5 == 0)

                return await this.context.Books.CountAsync() / 5;
            else

                return await this.context.Books.CountAsync() / 5 + 1;
        }

        public async Task<IReadOnlyCollection<Book>> GetCurrentPageBooks(int id)
        {
            if (id == 0)
            {
                id = 1;
            }
            if (id == 1)
            {
                return await this.context.Books
                    .Include(b => b.Status)
                    .Include(b => b.Grades)
                    .Include(b => b.Rental)
                    .Include(b => b.Reservations)
                    .Include(b => b.BookAuthors)
                        .ThenInclude(a => a.Author)
                    .Take(5)
                    .ToListAsync();
            }
            else
                return await this.context.Books
                    .Include(b => b.Status)
                    .Include(b => b.Grades)
                    .Include(b => b.Rental)
                    .Include(b => b.Reservations)
                    .Include(b => b.BookAuthors)
                        .ThenInclude(a => a.Author)
                    .Skip((id - 1) * 5)
                    .Take(5)
                    .ToListAsync();
        }
    }
}
