﻿using LMS.Data;
using LMS.Data.Data.Entities;
using LMS.ServiceLayer.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.ServiceLayer.Services
{
    public class CheckerService : ICheckerService
    {
        private readonly LMSDbContext context;
        private readonly IRentalManager rentalManager;
        private readonly INotificationService notificationService;

        public CheckerService(LMSDbContext context,
                              IRentalManager rentalManager,
                              INotificationService notificationService)
        {
            this.context = context;
            this.rentalManager = rentalManager;
            this.notificationService = notificationService;
        }

        public async Task ReturnBookIfDatePassed()
        {
            var rentals = this.context.Rentals.Where(r => r.ReservedDate.AddMinutes(10) < DateTime.Now);
            foreach (var rental in rentals)
            {
                await rentalManager.ReturnBookAsync(rental.UserId, rental.BookId);
                var bookTitle = context.Books.FirstOrDefault(b=>b.Id==rental.BookId).Title;
                var notification = await this.notificationService.CreateNotification(rental.UserId, $"The book with title [{bookTitle}] has been returned by the admin");
            }
        }
    }
}
