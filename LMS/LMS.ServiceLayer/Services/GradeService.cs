﻿using LMS.Data;
using LMS.Data.Data.Entities;
using LMS.ServiceLayer.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.ServiceLayer.Services
{
    public class GradeService : IGradeService
    {
        private readonly LMSDbContext context;

        public GradeService(LMSDbContext context)
        {
            this.context = context;
        }
        public async Task<Grade> Create(string bookId, string userId, double rating, string description)
        {

            var grade = new Grade()
            {
                BookId = bookId,
                Rating = rating,
                Review = description,
                UserId = userId,
            };

            await this.context.AddAsync(grade);
            await this.context.SaveChangesAsync();

            return grade;
        }
        public async Task<bool> CheckIfBookIsRated(string bookId, string userId)
        {
            return await this.context.Grades
                .AnyAsync(g => g.BookId == bookId && g.UserId == userId);
        }
        public async Task<Grade> GetGrade(string bookId, string userId)
        {
            var grade = await this.context.Grades
                .FirstOrDefaultAsync(g => g.BookId == bookId && g.UserId == userId);

            return grade;
        }
        //TODO Remove grade 
        public async Task<Grade> Edit(Grade grade)//<--
        {
            this.context.Grades.Update(grade);
            await this.context.SaveChangesAsync();

            return grade;
        }

        public async Task<IReadOnlyCollection<Grade>> GetGrades(string bookId)
        {
            var grades = await this.context.Grades
                .Include(g => g.Book)
                .Include(g => g.User)
                .Where(b => b.Book.Id == bookId)
                .ToListAsync();

            return grades;
        }
    }
}
