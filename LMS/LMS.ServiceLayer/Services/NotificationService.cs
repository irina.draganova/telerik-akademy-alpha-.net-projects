﻿using System;
using LMS.ServiceLayer.Contracts;
using System.Collections.Generic;
using System.Text;
using LMS.Data;
using LMS.Data.Data.Entities;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Globalization;

namespace LMS.ServiceLayer.Services
{
    public class NotificationService : INotificationService
    {
        private readonly LMSDbContext context;

        public NotificationService(LMSDbContext context)
        {
            this.context = context;
        }

        public async Task<Notification> CreateNotification(string userId, string description)
        {
            var notification = new Notification()
            {
                UserId = userId,
                Description = description,
                CreatedOn = DateTime.Now
            };

            await this.context.Notifications.AddAsync(notification);
            await this.context.SaveChangesAsync();

            return notification;
        }

        public async Task<ICollection<Notification>> GetNotifications(string receiverId)
        {
            var allNotifications = await context.Notifications
                .Where(u => u.Receiver.Id == receiverId)
                .OrderByDescending(n => n.CreatedOn)
                .ToListAsync();
           

            return allNotifications;
        }

        public async Task<Notification> ReadNotification(string notificationId)
        {
            var notification = await this.context.Notifications.FindAsync(notificationId);
            notification.IsRead = true;

            this.context.Update(notification);
            await this.context.SaveChangesAsync();

            return notification;
        }

        public async Task<Notification> SendNotificationToAdmin(string description)
        {
            var turboAdmin = await this.context.Users.FirstOrDefaultAsync(u => u.UserName == "admin");
            var notification = new Notification()
            {
                Description = description,
                UserId = turboAdmin.Id,
                CreatedOn = DateTime.Now
            };
            await this.context.Notifications.AddAsync(notification);
            await this.context.SaveChangesAsync();

            return notification;
        }
    }
}
