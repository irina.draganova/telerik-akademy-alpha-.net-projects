﻿using LMS.Data;
using LMS.Data.Data.Entities;
using LMS.ServiceLayer.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.ServiceLayer.Services
{
    public class RentalManager : IRentalManager
    {
        private readonly IBookService bookService;
        private readonly IReservationService reservationService;
        private readonly INotificationService notificationService;
        private readonly LMSDbContext context;

        public RentalManager(LMSDbContext context,
                             IBookService bookService,
                             IReservationService reservationService,
                             INotificationService notificationService)
        {
            this.bookService = bookService;
            this.reservationService = reservationService;
            this.context = context;
            this.notificationService = notificationService;

        }

        public async Task<Rental> GetRental(string userId, string bookId)
        {
            var rental = await context.Rentals.FirstOrDefaultAsync(r => r.BookId == bookId && r.UserId == userId);
            if (rental == null)
            {
                throw new Exception("You haven't taken that book");
            }
            return rental;
        }
        public async Task<Book> ChechkoutBookAsync(string userId, string bookId)
        {
            var book = await bookService.GetBook(bookId);
            if (context.Reservations.Any(r => r.BookId == bookId && r.UserId == userId))
            {
                throw new Exception("You cannot take a book that is already reserved");
            }
            if (context.Rentals.Any(r => r.UserId == userId && r.BookId == bookId))
            {
                throw new Exception("You have already taken that book");
            }
            if (context.Rentals.Any(r => r.BookId == bookId))
            {
                throw new Exception("The book is already taken");
            }
            if (book == null)
            {
                throw new Exception("Book not found!");
            }

            await context.Rentals.AddAsync(new Rental
            {
                BookId = bookId,
                UserId = userId,
                ReservedDate = DateTime.Now,
            });

            var status = context.Statuses.FirstOrDefault(s => s.Type == "Taken").Id;
            book.StatusId = status;
            book.RentalId = userId;

            this.context.Update(book);
            await context.SaveChangesAsync();


            var description = $"A book {book.Title} has been checkedout";
            await notificationService.SendNotificationToAdmin(description);

            return book;
        }
        public async Task<Book> ReturnBookAsync(string userId, string bookId)
        {
            var book = await bookService.GetBook(bookId);
            if (book == null)
            {
                throw new Exception("Book not found");
            }
            if (book.RentalId != userId)
            {
                throw new Exception("You cannot return a book u havent taken yet");
            }

            var rental = await GetRental(userId, bookId);
            this.context.Rentals.Remove(rental);

            bool isBookReserved = await context.Reservations.AnyAsync(r => r.BookId == bookId);
            if (isBookReserved)
            {
                var user = await this.context.Reservations.FirstOrDefaultAsync(r => r.BookId == bookId);
                await reservationService.RemoveReservation(user.UserId, bookId);

                await ChechkoutBookAsync(user.UserId, bookId);
                bool secondReservation = await context.Reservations.Select(r => r.BookId).AnyAsync();
                if (secondReservation)
                {
                    var status = context.Statuses.FirstOrDefault(s => s.Type == "Reserved").Id;
                    book.StatusId = status;
                    book.RentalId = user.UserId;
                }
            }
            else
            {
                var status = context.Statuses.FirstOrDefault(s => s.Type == "Available").Id;
                book.StatusId = status;
                book.RentalId = null;
            }

            context.Update(book);
            await context.SaveChangesAsync();

            var description = $"A book {book.Title} has been returned";
            await notificationService.SendNotificationToAdmin(description);

            return book;
        }
    }
}
