﻿using LMS.Data;
using LMS.Data.Data.Entities;
using LMS.ServiceLayer.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.ServiceLayer.Services
{
    public class ReservationService : IReservationService
    {
        private readonly LMSDbContext context;
        private readonly IBookService bookService;
        private readonly INotificationService notificationService;

        public ReservationService(LMSDbContext context, IBookService bookService, INotificationService notificationService)
        {
            this.context = context;
            this.bookService = bookService;
            this.notificationService = notificationService;
        }

        public async Task<Reservation> Create(string userId, string bookId)
        {
            var book = await bookService.GetBook(bookId);
            //TODO check bookId and userId
            var reservation = new Reservation()
            {
                ReservedDate = DateTime.Now,
                BookId = bookId,
                UserId = userId
            };

            book.StatusId = context.Statuses.FirstOrDefault(s => s.Type == "Reserved").Id;
            await context.Reservations.AddAsync(reservation);
            await context.SaveChangesAsync();

            var description = $"A book {book.Title} has been reserved";
            await notificationService.SendNotificationToAdmin(description);

            return reservation;
        }
        public async Task<Reservation> GetReservation(string userId, string bookId)
        {
            var reservation = await this.context.Reservations.FirstOrDefaultAsync(r => r.UserId == userId && r.BookId == bookId);

            return reservation;
        }
        public async Task<IReadOnlyCollection<Reservation>> GetReservationsForUserAsync(string id)
        {
            var reservations = await this.context.Reservations.Where(r => r.UserId == id).ToListAsync();
            return reservations;
        }
        public async Task RemoveReservation(string userId, string bookId)
        {
            var reservation = await GetReservation(userId, bookId);
            this.context.Reservations.Remove(reservation);
            await this.context.SaveChangesAsync();
        }
    }
}
