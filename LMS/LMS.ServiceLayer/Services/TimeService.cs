﻿using LMS.ServiceLayer.Contracts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LMS.ServiceLayer.Services
{
    public class TimeService : IHostedService
    {
        private readonly IServiceProvider serviceProvider;
        private Timer timer;

        public TimeService(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            this.timer = new Timer(Checker, null, TimeSpan.Zero, TimeSpan.FromMinutes(1));
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            this.timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }

        private async void Checker(object state)
        {
            using (var scope = this.serviceProvider.CreateScope())
            {
                var asd = scope.ServiceProvider.GetRequiredService<ICheckerService>();
                await asd.ReturnBookIfDatePassed();
            }
        }
    }
}
