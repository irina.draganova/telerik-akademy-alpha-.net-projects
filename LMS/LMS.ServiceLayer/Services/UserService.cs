﻿using LMS.Data;
using LMS.Data.Data.Entities;
using LMS.ServiceLayer.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMS.ServiceLayer.Services
{
    public class UserService : IUserService
    {
        private readonly LMSDbContext context;

        public UserService(LMSDbContext context)
        {
            this.context = context;
        }

        [HttpPost]
        public async Task<Ban> CreateBan(string userId, string reason, int days)
        {
            var admin = await this.context.Users.FirstOrDefaultAsync(u => u.UserName == "admin");
            if (userId == admin.Id)
            {
                throw new Exception("Admin cannot be banned!");
            }
            var ban = new Ban()
            {
                Reason = reason,
                ExpirationDate = DateTime.Now.AddDays(days),
                UserId = userId
            };

            this.context.Bans.Add(ban);
            await this.context.SaveChangesAsync();

            var user = await FindUserAsync(userId);
            user.BanId = ban.Id;
            this.context.Update(user);
            await this.context.SaveChangesAsync();

            return ban;
        }

        public async Task<User> BanUser(string userId)
        {
            var user = await FindUserAsync(userId);
            var ban = await FindBanAsync(userId);

            user.BanId = ban.Id;
            this.context.Update(user);
            await this.context.SaveChangesAsync();
         
            return user;
        }

        public async Task DeleteBan(string userId)
        {
            var user = await FindUser(userId);
            var ban = await FindBanAsync(userId);
            this.context.Bans.Remove(ban);

            user.BanId = null;
            this.context.Update(user);

            this.context.SaveChanges();
        }

        public async Task<IReadOnlyCollection<Ban>> GetAllBans(string userId)
        {
            var allBans = await this.context.Bans.ToListAsync();

            return allBans;
        }

        public async Task<IReadOnlyCollection<User>> GetAllUsers()
        {
            var allUsers = await this.context.Users
                .Include(u => u.Ban)
                .ToListAsync();

            return allUsers;
        }

        public IReadOnlyCollection<User> GetUsers(string name)
        {
            return this.context.Users
                .Where(u => u.UserName.Contains(name, StringComparison.OrdinalIgnoreCase))
               .ToList();
        }

        public async Task<User> FindUserAsync(string userId)
        {
            var user = await context.Users.FirstOrDefaultAsync(u => u.Id == userId);

            return user;
        }
        public async Task<User> FindUser(string userId)
        {
            var user = await context.Users.FirstOrDefaultAsync(u => u.Id == userId);

            return user;
        }
        public async Task<Ban> FindBanAsync(string userId)
        {
            var ban = await context.Bans.FirstOrDefaultAsync(b => b.UserId == userId);

            return ban;
        }

        public Ban FindBan(string userId)
        {
            var ban = context.Bans.FirstOrDefault(b => b.UserId == userId);

            return ban;
        }
    }
}