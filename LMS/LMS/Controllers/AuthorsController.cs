﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LMS.Data;
using LMS.Data.Data.Entities;
using Microsoft.AspNetCore.Authorization;
using LMS.Models;
using LMS.Infrastructure.Mappers.Contracts;
using LMS.ServiceLayer.Services;

namespace LMS.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AuthorsController : Controller
    {
        private readonly LMSDbContext context;
        private readonly IAuthorService authorService;

        public AuthorsController(LMSDbContext context,IAuthorService authorService)/*, IViewModelMapper<Author,AuthorViewModel> authorVMMapper*/
        {
            this.context = context;
            this.authorService = authorService;
        }

        // GET: Authors
        public async Task<IActionResult> Index()
        {
            return View(await authorService.GetAuthorsAsync());
        }

        // GET: Authors/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var author = await authorService.GetAuthorAsync(id);
            if (author == null)
            {
                return NotFound();
            }

            return View(author);
        }

        // GET: Authors/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Authors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //public async Task<IActionResult> Create(AuthorViewModel authorViewModel)
        //{
        //    authorvm
        //    if (ModelState.IsValid)
        //    {
        //        _context.Add(author);
        //        await _context.SaveChangesAsync();
        //        return RedirectToAction(nameof(Index));
        //    }
        //    return View(author);
        //}
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AuthorViewModel authorViewModel)
        {
            var author = await authorService.CreateAuthorAsync(authorViewModel.Name);
            if (ModelState.IsValid)
            {
                return RedirectToAction(nameof(Index));
            }
            return View(author);
        }

        // GET: Authors/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var author = await authorService.GetAuthorAsync(id);
            if (author == null)
            {
                return NotFound();
            }

            return View(author);
        }

        // POST: Authors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, AuthorViewModel authorViewModel)
        {
            var author = await authorService.GetAuthorAsync(id);
            author.Name = authorViewModel.Name;

            if (ModelState.IsValid)
            {
                try
                {
                    await authorService.Edit(author);
                }
                catch (DbUpdateConcurrencyException)
                {
                        return NotFound();
                }
                return RedirectToAction(nameof(Index));
            }

            return View(author);
        }

        // GET: Authors/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var author = await authorService.GetAuthorAsync(id);

            return View(author);
        }

        // POST: Authors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            await this.authorService.DeleteAuthor(id);

            return RedirectToAction(nameof(Index));
        }
    }
}
