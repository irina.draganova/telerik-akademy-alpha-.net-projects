﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using LMS.Data;
using LMS.Data.Data.Entities;
using LMS.Infrastructure.Mappers.Contracts;
using LMS.Models;
using LMS.ServiceLayer.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace LMS.Controllers
{
    [Authorize(Roles = "Admin")]
    public class BanController : Controller
    {
        private readonly IUserService service;
        private readonly UserManager<User> userManager;
        private readonly IViewModelMapper<IReadOnlyCollection<User>, CollectionViewModel> allUsersMapper;

        public BanController(IUserService service, UserManager<User> userManager,
            IViewModelMapper<IReadOnlyCollection<User>, CollectionViewModel> allUsersMapper)
        {
            this.service = service;
            this.userManager = userManager;
            this.allUsersMapper = allUsersMapper;
        }

        [HttpGet]
        public async Task<IActionResult> CreateBan(string id)
        {
            try
            {
                var user = await this.userManager.FindByIdAsync(id);
                if (user.UserName == "admin")
                {
                    throw new ArgumentException("Admin cannot be banned!");
                }
                var banViewModel = new BanViewModel()
                {
                    UserId = id,
                    UserName = user.UserName
                };

                return View(banViewModel);
            }
            catch (Exception ex)
            {
                var vm = new ErrorViewModel();
                vm.ErrorDescription = ex.Message;

                return RedirectToAction("Index", "ErrorHandler", vm);
            }

        }

        [HttpPost]
        public async Task<IActionResult> CreateBan(BanViewModel banModel, string id)
        {
            try
            {
                await this.service.CreateBan(id, banModel.Reason, banModel.Days);

                return this.RedirectToAction("BannedUsers", "Ban");
            }
            catch (Exception ex)
            {
                var vm = new ErrorViewModel();
                vm.ErrorDescription = ex.Message;

                return RedirectToAction("Index", "ErrorHandler", vm);
            }
            
        }

        public async Task<IActionResult> BannedUsers()
        {
            var allUsers = await this.service.GetAllUsers();
            var usersViewModel = allUsersMapper.MapFrom(allUsers);

            return View(usersViewModel);
        }

       // [HttpGet("/Ban/Unban/{Id}")]
        public IActionResult Unban(string id)
        {
            this.service.DeleteBan(id);

            return this.RedirectToAction("Index", "Home");
        }

        public IActionResult UnbannedUsers()
        {
            return View();
        }
    }
}