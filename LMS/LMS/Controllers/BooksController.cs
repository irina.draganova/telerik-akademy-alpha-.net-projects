﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LMS.Data;
using LMS.Data.Data.Entities;
using LMS.Models;
using LMS.ServiceLayer.Contracts;
using LMS.Infrastructure.Extensions;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using LMS.Infrastructure.Mappers.Contracts;
using System.Threading;

namespace LMS.Controllers
{
    public class BooksController : Controller
    {
        private readonly LMSDbContext _context;//da se maha skoro
        private readonly IBookService bookService;
        private readonly IRentalManager rentalManager;
        private readonly IReservationService reservationService;
        private readonly IGradeService gradeService;
        private readonly IViewModelMapper<IReadOnlyCollection<Book>, CollectionViewModel> allBooksMapper;

        public BooksController(LMSDbContext context,
                               IBookService bookService,
                               IRentalManager rentalManager,
                               IReservationService reservationService,
                               IGradeService gradeService,
                               IViewModelMapper<IReadOnlyCollection<Book>, CollectionViewModel> allBooksMapper)
        {
            _context = context;
            this.bookService = bookService ?? throw new ArgumentNullException(nameof(bookService));
            this.rentalManager = rentalManager ?? throw new ArgumentNullException(nameof(rentalManager));
            this.reservationService = reservationService ?? throw new ArgumentNullException(nameof(reservationService));
            this.gradeService = gradeService;
            this.allBooksMapper = allBooksMapper ?? throw new ArgumentNullException(nameof(allBooksMapper));
        }

        [Authorize]
        public async Task<IActionResult> MyBooks()
        {
            string userId = this.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var books = await bookService.GetUserBooks(userId);
            var takenModel = allBooksMapper.MapFrom(books);

            var reservedBooks = await bookService.GetReservedBooksByUser(userId);
            var reservedModel = allBooksMapper.MapFrom(reservedBooks);

            foreach (var item in takenModel.Books)
            {
                item.CurrentUserId = userId;
            }
            var myBooksModel = new MyBooksViewModel()
            {
                ReservedBooks = reservedModel,
                TakenBooks = takenModel
            };

            return View(myBooksModel);
        }

        [Authorize]
        public async Task<IActionResult> Checkout(string bookId)
        {
            string userid = this.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            try
            {
                var book = await rentalManager.ChechkoutBookAsync(userid, bookId);

                return Redirect("MyBooks");
            }
            catch (Exception ex)
            {
                var vm = new ErrorViewModel();
                vm.ErrorDescription = ex.Message;

                return RedirectToAction("Index", "ErrorHandler", vm);
            }
        }
        [Authorize]
        public async Task<IActionResult> Return(string bookId)
        {
            string userid = this.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var book = await rentalManager.ReturnBookAsync(userid, bookId);

            return Redirect("Details/" + book.Id);
        }

        [Authorize]
        public async Task<IActionResult> Reserve(string bookId)
        {
            string userid = this.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var reservation = await reservationService.Create(userid, bookId);

            return Redirect("MyBooks");
        }

        [HttpGet] //("/Book/Details/{Id}")
        public async Task<IActionResult> GetReviews(string id) // <-- bookId
        {
            // TODO REMOVE!
            //Thread.Sleep(2000);

            var grades = await this.gradeService.GetGrades(id);


            var gradesViewModel = new List<GradeViewModel>();

            foreach (var item in grades)
            {
                var vm = new GradeViewModel()
                {
                    Title = item.Book.Title,
                    Rating = item.Rating,
                    Review = item.Review,
                    ReviewAuthor = item.User.UserName
                };
                gradesViewModel.Add(vm);
            }

            //return View(gradesViewModel);
            //return Json(gradesViewModel);
            return PartialView("GetReviews", gradesViewModel);
        }

        public async Task< IActionResult> GetJsReviews()
        {
            var books = await this.bookService.GetBooks();

            return Json(books);
        }

        [Authorize]
        // GET: Books/Details/5
        public async Task<IActionResult> Details(BookViewModel bookVM)
        {

            var book = await bookService.GetBook(bookVM.Id);

            var bookvm = book.MapFrom();

            return View(bookvm);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            ViewData["StatusId"] = new SelectList(_context.Statuses, "Id", "Id");
            ViewData["AuthorsNames"] = new SelectList(_context.Authors.Select(a => a.Name).ToList());

            return View();
        }

        public IActionResult Reviews()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateBookViewModel createBookViewModel)
        {
            //var book = createBookVMMapper.MapFrom(createBookViewModel);
            //var createdbook = await bookService.CreateBook(book);
            var book = await bookService
                .CreateBook(createBookViewModel.Title, createBookViewModel.Year, createBookViewModel.AuthorsNames);

            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = "Admin")]
        // GET: Books/Edit/5
        //TODO REMOVE
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Books.FindAsync(id);
            if (book == null)
            {
                return NotFound();
            }
            ViewData["StatusId"] = new SelectList(_context.Statuses, "Id", "Id", book.StatusId);
            return View(book);
        }

        //TODO
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,Title,Year,RentalId,StatusId")] Book book)
        {
            if (id != book.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(book);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookExists(book.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["StatusId"] = new SelectList(_context.Statuses, "Id", "Id", book.StatusId);
            return View(book);
        }

        [Authorize(Roles = "Admin")]
        // GET: Books/Delete/5
        //TODO
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Books
                .Include(b => b.Status)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (book == null)
            {
                return NotFound();
            }

            return View(book);
        }

        // POST: Books/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var book = await _context.Books.FindAsync(id);
            _context.Books.Remove(book);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BookExists(string id)
        {
            return _context.Books.Any(e => e.Id == id);
        }
        public async Task<IActionResult> Index(string sortOrder, string searchString, int Id=1)
        {
            CollectionViewModel model;
            var currentPageBooks = await this.bookService.GetCurrentPageBooks(Id);
            var allBooks = await bookService.GetBooks();
            string userid = this.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            ViewData["NameSortParam"] = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewData["DateSortParam"] = sortOrder == "date_desc" ? "Date" : "date_desc";
            ViewData["RatingSortParam"] = sortOrder == "rate_desc" ? "Rating" : "rate_desc";
            ViewData["AuthorSortParam"] = sortOrder == "Author" ? "auth_desc" : "Author";
            ViewData["StatusSortParam"] = sortOrder == "Status" ? "stat_desc" : "Status";

            ViewData["CurrentFilter"] = searchString;

            if (!String.IsNullOrEmpty(searchString))
            {
                model = allBooksMapper.MapFrom(allBooks);
                var lower = searchString.ToLower();
                model.Books = model.Books.Where(b => b.Title.ToLower().Contains(lower)
                                       || b.AuthorsNames.Any(item => item.ToLower().Contains(lower))).ToList();
                model.IsSearchApplied = true;
            }
            else
            {
                model = allBooksMapper.MapFrom(currentPageBooks);
                model.IsSearchApplied = false;
            }
            model.CurrentPage = Id;
            model.PreviousPage = Id == 1 ? 1 : Id - 1;
            model.NextPage = Id + 1;
            model.TotalPages = await this.bookService.GetPagesCount();
            if (model.NextPage > model.TotalPages)
                model.NextPage = model.TotalPages;

            foreach (var item in model.Books)
            {
                item.CurrentUserId = userid;
            }

            switch (sortOrder)
            {
                case "name_desc":
                    model.Books = model.Books.OrderByDescending(s => s.Title).ToList();
                    break;
                case "Date":
                    model.Books = model.Books.OrderBy(s => s.Year).ToList();
                    break;
                case "date_desc":
                    model.Books = model.Books.OrderByDescending(s => s.Year).ToList();
                    break;
                case "Rating":
                    model.Books = model.Books.OrderBy(b => b.Rating).ToList();
                    break;
                case "rate_desc":
                    model.Books = model.Books.OrderByDescending(b => b.Rating).ToList();
                    break;
                case "Author":
                    model.Books = model.Books.OrderBy(b => b.AuthorsNames.First()).ToList();
                    break;
                case "auth_desc":
                    model.Books = model.Books.OrderByDescending(b => b.AuthorsNames.First()).ToList();
                    break;
                case "Status":
                    model.Books = model.Books.OrderBy(b => b.Status).ToList();
                    break;
                case "stat_desc":
                    model.Books = model.Books.OrderByDescending(b => b.Status).ToList();
                    break;
                default:
                    model.Books = model.Books.OrderBy(s => s.Title).ToList();
                    break;
            }

            return View(model);
        }

    }
}
