﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.Models;
using Microsoft.AspNetCore.Mvc;

namespace LMS.Controllers
{
    public class ErrorHandlerController : Controller
    {
        public IActionResult Index(ErrorViewModel vm)
        {
            return View(vm);
        }
    }
}