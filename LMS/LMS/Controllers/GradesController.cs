﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using LMS.Data.Data.Entities;
using LMS.Infrastructure.Mappers.Contracts;
using LMS.Models;
using LMS.ServiceLayer.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace LMS.Controllers
{
    public class GradesController : Controller
    {
        private readonly IGradeService gradeService;
        private readonly IViewModelMapper<Grade, GradeViewModel> gradeViewModelMapper;
        private readonly IViewModelMapper<GradeViewModel, Grade> grademodelMapper;

        public GradesController(IGradeService gradeService,
                                IViewModelMapper<Grade, GradeViewModel> gradeViewModelMapper,
                                IViewModelMapper<GradeViewModel, Grade> grademodelMapper)
        {

            this.gradeService = gradeService ?? throw new ArgumentNullException(nameof(gradeService));
            this.gradeViewModelMapper = gradeViewModelMapper ?? throw new ArgumentNullException(nameof(gradeViewModelMapper));
            this.grademodelMapper = grademodelMapper;
        }
        public IActionResult Index()
        {

            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Review(string Id)
        {
            try
            {
                var userId = this.User.FindFirst(ClaimTypes.NameIdentifier).Value;
                if (await gradeService.CheckIfBookIsRated(Id, userId))
                {
                    throw new Exception("You have already reviewed this book!");
                }
            }
            catch (Exception ex)
            {
                var vm = new ErrorViewModel();
                vm.ErrorDescription = ex.Message;

                return RedirectToAction("Index", "ErrorHandler", vm);
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Review(GradeViewModel gradeViewModel)
        {
            var userId = this.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var grade = await this.gradeService.Create(gradeViewModel.Id, userId, gradeViewModel.Rating, gradeViewModel.Review);

            return RedirectToAction("Index", "Books");
        }
        [HttpGet]
        public async Task<IActionResult> Edit(string Id)
        {
            var userId = this.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var grade = await gradeService.GetGrade(Id, userId);
            var gradeVM = gradeViewModelMapper.MapFrom(grade);

            return View(gradeVM);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(GradeViewModel gradeViewModel)
        {
            var userId = this.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            gradeViewModel.UserId = userId;
            var grade = grademodelMapper.MapFrom(gradeViewModel);
            var editedGrade = await gradeService.Edit(grade);

            return RedirectToAction("Index", "Books");
        }
    }


}

//https://localhost:44343/Grades/Edit/758daecf-2462-44ed-8e59-b95a3657b7b5