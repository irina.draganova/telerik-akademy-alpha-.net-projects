﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LMS.Models;
using LMS.ServiceLayer.Contracts;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using LMS.Infrastructure.Mappers.Contracts;
using LMS.Data.Data.Entities;

namespace LMS.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserService userService;
        private readonly IBookService bookService;
        private readonly IViewModelMapper<Book, BookViewModel> bookMapper;

        public HomeController(IUserService userService, IBookService bookService, IViewModelMapper<Book, BookViewModel> bookMapper)
        {
            this.userService = userService;
            this.bookService = bookService;
            this.bookMapper = bookMapper;
        }
        
        public async Task<IActionResult> Index()
        {
            if (this.User.Identity.IsAuthenticated)
            {
                var allusers = await this.userService.GetAllUsers();
                var allBooks = await this.bookService.GetBooks();

                var viewModel = new IndexViewModel();
                var usersViewModel = new List<UserViewModel>();

                // todo: filter in entity framework
                foreach (var user in allusers.Where( u => u.BanId == null && u.UserName != "admin"))
                {
                    usersViewModel.Add(new UserViewModel()
                    {
                        UserId = user.Id,
                        UserName = user.UserName
                    });
                }

                var booksViewModel = new List<BookViewModel>();

                foreach (var book in allBooks)
                {
                    booksViewModel.Add(bookMapper.MapFrom(book));
                }
                viewModel.Books = booksViewModel;
                viewModel.Users = usersViewModel;

                return View(viewModel);
            }
            return View();
        }

        public IActionResult Users([FromQuery]string name)
        {
            var users = this.userService
                .GetUsers(name)
                .Select(u => new UserViewModel(u.UserName))
                .ToList();

            return PartialView("_SearchPartial", users);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
