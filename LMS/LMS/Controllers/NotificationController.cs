﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LMS.ServiceLayer.Services;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using LMS.Models;
using System.Globalization;
using LMS.Data.Data.Entities;
using Microsoft.AspNetCore.Authorization;

namespace LMS.Controllers
{
    public class NotificationController : Controller
    {
        private readonly INotificationService notificationService;

        public NotificationController(INotificationService notificiationService)
        {
            this.notificationService = notificiationService;
        }
        [Authorize]
        public async Task<IActionResult> Index()
        {
            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var nots = await this.notificationService.GetNotifications(userId);
            //TODO mapper here
            var notsViewModel = new List<NotificationViewModel>();

            foreach (var not in nots.Where(n => n.IsRead == false))
            {
                notsViewModel.Add(new NotificationViewModel()
                {
                    Description = not.Description,
                    Id = not.Id,
                    CreatedOn = not.CreatedOn.ToString()
                });
            }

            return View(notsViewModel);
        }

        [Authorize]
        [HttpGet("/Notification/Read/{Id}")]
        public async Task<IActionResult> Read(string id) // <-- userId
        {
            var notification = await this.notificationService.ReadNotification(id);

            return this.RedirectToAction("Seen", "Notification");
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Seen()
        {
            var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
            var notifications = await this.notificationService.GetNotifications(userId);
            var seenNotification = notifications.Where(n => n.IsRead == true).ToList();

            var seenNotificationVewModel = new List<NotificationViewModel>();

            //TODO mapper
            foreach (var item in seenNotification)
            {
                seenNotificationVewModel.Add(new NotificationViewModel()
                {
                    Description = item.Description,
                    CreatedOn = item.CreatedOn.ToString()
                });
            }

            return View(seenNotificationVewModel);
        }
    }
}