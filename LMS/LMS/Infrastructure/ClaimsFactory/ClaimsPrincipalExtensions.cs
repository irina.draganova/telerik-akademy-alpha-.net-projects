﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace LMS.Infrastructure.ClaimsFactory
{
    public static class ClaimsPrincipalExtensions
    {
        public static bool IsBanned(this ClaimsPrincipal user)
        {
            return bool.Parse(user.FindFirst("IsBanned")?.Value ?? "False");
        }
    }
}
