﻿using LMS.Data.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace LMS.Infrastructure.ClaimsFactory
{
    public class CustomClaimsPrincipalFactory : UserClaimsPrincipalFactory<User, IdentityRole>
    {
        public CustomClaimsPrincipalFactory(UserManager<User> userManager, RoleManager<IdentityRole> roleManager,IOptions<IdentityOptions> optionsAccessor) : base(userManager, roleManager, optionsAccessor)
        {
        }

        public override async Task<ClaimsPrincipal> CreateAsync(User user)
        {
            var claimsPrincipal = await base.CreateAsync(user);
            bool isBanned = user.BanId != null;

            ((ClaimsIdentity)claimsPrincipal.Identity)
                .AddClaim(new Claim("IsBanned", isBanned.ToString()));

            return claimsPrincipal;
        }
    }
}
