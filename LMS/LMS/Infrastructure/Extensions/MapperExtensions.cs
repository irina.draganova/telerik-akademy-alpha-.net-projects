﻿using LMS.Data.Data.Entities;
using LMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Infrastructure.Extensions
{
    public static class MapperExtensions
    {
        public static BookViewModel MapFrom(this Book book)
        {
            return new BookViewModel()
            {
                Id = book.Id,
                Name = book?.BookAuthors?.FirstOrDefault(a => a.BookId == book.Id)?.Author?.Name,
                AuthorsNames = book.BookAuthors.Where(ba => ba.BookId == book.Id).Select(a => a.Author.Name).ToList(),
                Title = book.Title,
                Status = book.Status.Type,
                Year = book.Year,
                UserId = book.RentalId,
                Rating = book.Grades.Average(grade => grade.Rating)
                //BookAuthors = book.Authors.MapFrom(),
            };
        }
        public static BookAuthorViewModel MapFrom(this BookAuthor bookAuthor)
        {
            return new BookAuthorViewModel()
            {
                Name = bookAuthor.Author.Name,
                Title = bookAuthor.Book.Title,
                Year = bookAuthor.Book.Year,
                BookId = bookAuthor.BookId,
                AuthorId = bookAuthor.AuthorId,
                
            };
        }
        public static ICollection<BookAuthorViewModel> MapFrom(this ICollection<BookAuthor> bookAuthors)
        {
            return bookAuthors.Select(ba => ba.MapFrom()).ToList();
        }

        public static ICollection<BookViewModel> MapFrom(this ICollection<Book> books)
        {
            return books.Select(b => b.MapFrom()).ToList();
        }
    }
}
