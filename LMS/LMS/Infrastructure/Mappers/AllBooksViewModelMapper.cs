﻿using LMS.Data.Data.Entities;
using LMS.Infrastructure.Mappers.Contracts;
using LMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Infrastructure.Mappers
{
    public class AllBooksViewModelMapper : 
        IViewModelMapper<IReadOnlyCollection<Book>, CollectionViewModel>
    {
        private readonly IViewModelMapper<Book, BookViewModel> bookMapper;
        public AllBooksViewModelMapper(IViewModelMapper<Book, BookViewModel> bookMapper)
        {
            this.bookMapper = bookMapper ?? throw new ArgumentNullException(nameof(bookMapper));
        }
        public CollectionViewModel MapFrom(IReadOnlyCollection<Book> entity)
        {
            return new CollectionViewModel
            {
                Books = entity.Select(this.bookMapper.MapFrom).ToList()
            };
        }
    }
}
