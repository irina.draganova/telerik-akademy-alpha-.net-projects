﻿using LMS.Data.Data.Entities;
using LMS.Infrastructure.Mappers.Contracts;
using LMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Infrastructure.Mappers
{
    public class AllUsersViewModelMapper :
        IViewModelMapper<IReadOnlyCollection<User>, CollectionViewModel>
    {
        private readonly IViewModelMapper<User, UserViewModel> userMapper;
        public AllUsersViewModelMapper(IViewModelMapper<User, UserViewModel> userMapper)
        {
            this.userMapper = userMapper ?? throw new ArgumentNullException(nameof(userMapper));
        }
        public CollectionViewModel MapFrom(IReadOnlyCollection<User> entity)
        {
            return new CollectionViewModel
            {
                Users = entity.Select(this.userMapper.MapFrom).ToList()
            };
        }
    }
}
