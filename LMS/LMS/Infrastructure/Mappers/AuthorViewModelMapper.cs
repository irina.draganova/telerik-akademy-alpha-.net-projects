﻿using LMS.Data.Data.Entities;
using LMS.Infrastructure.Mappers.Contracts;
using LMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Infrastructure.Mappers
{
    public class AuthorViewModelMapper : IViewModelMapper<Author, AuthorViewModel>
    {
        public AuthorViewModel MapFrom(Author entity)
        {
            var vm = new AuthorViewModel
            {
                Name = entity.Name,
            };
            return vm;
        }
    }
}
