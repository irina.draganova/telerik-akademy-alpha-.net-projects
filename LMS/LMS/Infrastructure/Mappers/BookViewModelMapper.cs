﻿using LMS.Data.Data.Entities;
using LMS.Infrastructure.Mappers.Contracts;
using LMS.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace LMS.Infrastructure.Mappers
{
    public class BookViewModelMapper : IViewModelMapper<Book, BookViewModel>
    {
        public BookViewModel MapFrom(Book entity)
        {

            var bookVM = new BookViewModel
            {
                Id = entity.Id,
                Name = entity?.BookAuthors?.FirstOrDefault(a => a.BookId == entity.Id)?.Author?.Name,
                AuthorsNames = entity.BookAuthors?.Where(ba => ba.BookId == entity.Id)?.OrderBy(a=>a.Author.Name).Select(a => a.Author.Name)?.ToList(),
                Title = entity.Title,
                Status = entity.Status?.Type,
                Year = entity.Year,
                UserId=entity.RentalId,
            };
            if (entity.Grades.Where(a => a.BookId == entity.Id).Any())
            {
                bookVM.Rating = entity.Grades.Where(a => a.BookId == entity.Id).Average(g => g.Rating);
            }
            else
                bookVM.Rating = 0;

            return bookVM;
        }
    }
}
