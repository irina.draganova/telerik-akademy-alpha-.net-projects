﻿using LMS.Data;
using LMS.Data.Data.Entities;
using LMS.Infrastructure.Mappers.Contracts;
using LMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Infrastructure.Mappers
{
    public class CreateBookViewModelMapper : IViewModelMapper<CreateBookViewModel, Book>
    {
        //private readonly LMSDbContext context;

        //public CreateBookViewModelMapper(LMSDbContext context)
        //{
        //    this.context = context;
        //}
        public Book MapFrom(CreateBookViewModel entity)
        {
            var book = new Book();
            //foreach (var item in entity.AuthorsNames)
            //{
            //    var bas = context.BookAuthors.FirstOrDefault(ba => ba.Author.Name == item);
            //    if (bas != null)
            //    {
            //        book.BookAuthors.Add(bas);
            //    }
            //}
            book.Title = entity.Title;
            book.Year = entity.Year;

            return book;
        }
    }
}
