﻿using LMS.Data.Data.Entities;
using LMS.Infrastructure.Mappers.Contracts;
using LMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Infrastructure.Mappers
{
    public class GradeViewModelMapper : IViewModelMapper<Grade, GradeViewModel>
        , IViewModelMapper<GradeViewModel, Grade>
    {
        public GradeViewModel MapFrom(Grade entity)
        {
            var gradeVM = new GradeViewModel
            {
                Rating = entity.Rating,
                Review= entity.Review
            };

            return gradeVM;
        }
        public Grade MapFrom(GradeViewModel entity)
        {
            var grade = new Grade
            {
                UserId = entity.UserId,
                BookId = entity.Id,
                Rating = entity.Rating,
                Review = entity.Review
            };

            return grade;
        }
    }
}
