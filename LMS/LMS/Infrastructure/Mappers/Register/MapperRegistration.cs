﻿using LMS.Data.Data.Entities;
using LMS.Infrastructure.Mappers.Contracts;
using LMS.Models;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Infrastructure.Mappers.Register
{
    public static class MapperRegistration
    {
        public static IServiceCollection AddCustomMappers(this IServiceCollection services)
        {
            services.AddSingleton<IViewModelMapper<Book, BookViewModel>, BookViewModelMapper>();
            services.AddSingleton<IViewModelMapper<Grade, GradeViewModel>, GradeViewModelMapper>();
            services.AddSingleton<IViewModelMapper<GradeViewModel, Grade>, GradeViewModelMapper>();

          //services.AddSingleton<IViewModelMapper<GradeViewModel, Grade>, GradeViewModelMapper>();
            services.AddSingleton<IViewModelMapper<User, UserViewModel>, UserViewModelMapper>();
          //services.AddSingleton<IViewModelMapper<CreateBookViewModel, Book>, CreateBookViewModelMapper>();
                                //IViewModelMapper<CreateBookViewModel, Book>
            services.AddSingleton<IViewModelMapper<IReadOnlyCollection<Book>, CollectionViewModel>, AllBooksViewModelMapper>();
            services.AddSingleton<IViewModelMapper<IReadOnlyCollection<User>, CollectionViewModel>, AllUsersViewModelMapper>();

            return services;
        }
    }
}
