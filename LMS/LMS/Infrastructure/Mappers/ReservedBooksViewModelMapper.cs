﻿using LMS.Data.Data.Entities;
using LMS.Infrastructure.Mappers.Contracts;
using LMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Infrastructure.Mappers
{
    public class ReservedBooksViewModelMapper : IViewModelMapper<IReadOnlyCollection<Book>, CollectionViewModel>,                                                IViewModelMapper<Book, ReservedBooksViewModel>
    {
        private readonly IViewModelMapper<Book, BookViewModel> bookMapper;
        public ReservedBooksViewModelMapper(IViewModelMapper<Book, BookViewModel> bookMapper)
        {
            this.bookMapper = bookMapper ?? throw new ArgumentNullException(nameof(bookMapper));
        }
        public CollectionViewModel MapFrom(IReadOnlyCollection<Book> entity)
        {
            return new CollectionViewModel
            {
                Books = entity.Select(this.bookMapper.MapFrom).Where(x => x.CurrentUserId == null).ToList()
            };
        }

        public ReservedBooksViewModel MapFrom(Book entity)
        {
            var reservedBookVm = new ReservedBooksViewModel()
            {
                Id = entity.Id,
                AuthorsNames = entity.BookAuthors?.Where(ba => ba.BookId == entity.Id)?.OrderBy(a => a.Author.Name).Select(a => a.Author.Name)?.ToList(),
                Title = entity.Title,
                Status = entity.Status?.Type,
                Year = entity.Year,
                //UserId = entity.Reservations.Where(r=>r.UserId),
            };
            if (entity.Grades.Where(a => a.BookId == entity.Id).Any())
            {
                reservedBookVm.Rating = entity.Grades.Where(a => a.BookId == entity.Id).Average(g => g.Rating);
            }
            else
                reservedBookVm.Rating = 0;

            return reservedBookVm;

        }
    }
}

