﻿using LMS.Data.Data.Entities;
using LMS.Infrastructure.Mappers.Contracts;
using LMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Infrastructure.Mappers
{
    public class UserViewModelMapper : IViewModelMapper<User, UserViewModel>
    {
        public UserViewModel MapFrom(User entity)
        {
            return new UserViewModel()
            {
                UserId = entity.Id,
                UserName = entity.UserName,
                BanId = entity.BanId

            };
    }
    }
}
