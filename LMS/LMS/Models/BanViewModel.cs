﻿using LMS.Data.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Models
{
    public class BanViewModel
    {
        public string BanId { get; set; }   
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Reason { get; set; }
        public int Days { get; set; }
    }
}
