﻿    using System.ComponentModel.DataAnnotations;

namespace LMS.Models
{
    public class BookAuthorViewModel
    {
        public string AuthorId { get; set; }
        public string BookId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required(ErrorMessage = "The Title field is required.")]
        public string Title { get; set; }

        [Required]
        public int Year { get; set; }
    }
}