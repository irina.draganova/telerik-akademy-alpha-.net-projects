﻿using LMS.Data.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Models
{
    public class BookViewModel
    {
        public string Id { get; set; }

        [Required(ErrorMessage ="Select title")]
        public string Title { get; set; }
        public string Name { get; set; }

        [Required(ErrorMessage = "Select authors")]
        public List<string> AuthorsNames { get; set; }
        public string UserId { get; set; }
        public string CurrentUserId { get; set; }
        public int Year { get; set; }
        public string Status { get; set; }
        public List<string> Reviews { get; set; }

        [Required]
        public double Rating { get; set; }
        public bool IsRated { get; set; }
    }
}
