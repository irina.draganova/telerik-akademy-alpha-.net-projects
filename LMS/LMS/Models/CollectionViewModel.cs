﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Models
{
    public class CollectionViewModel
    {
        public IReadOnlyCollection<BookViewModel> Books { get; set; }
        public IReadOnlyCollection<UserViewModel> Users { get; set; }

        public int? PreviousPage { get; set; }

        public int CurrentPage { get; set; }

        public int? NextPage { get; set; }

        public int TotalPages { get; set; }

        public bool IsSearchApplied { get; set; }
    }
}
