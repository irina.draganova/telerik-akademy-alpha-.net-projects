﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Models
{
    public class CreateBookViewModel
    {
        //(bookViewModel.Title, bookViewModel.Year, bookViewModel.AuthorsNames)
        public string Title { get; set; }
        public int Year { get; set; }
        public List<string> AuthorsNames { get; set; }
    }
}
