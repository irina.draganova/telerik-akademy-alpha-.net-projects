﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Models
{
    public class DeleteGradeViewModel
    {
        public string UserId { get; set; }
        public string Id { get; set; }
    }
}
