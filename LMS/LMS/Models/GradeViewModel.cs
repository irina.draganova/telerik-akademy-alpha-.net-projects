﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Models
{
    public class GradeViewModel
    {
        public string Id { get; set; }

        [Required]
        [Range(1, 10)]
        public double Rating { get; set; }
        public string Review { get; set; }
        public string UserId { get; set; }

        public string ReviewAuthor { get; set; }

        public string Title { get; set; }
    }
}
