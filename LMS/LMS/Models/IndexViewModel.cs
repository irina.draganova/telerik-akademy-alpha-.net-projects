﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Models
{
    public class IndexViewModel
    {
        public IReadOnlyCollection<UserViewModel> Users { get; set; }

        public IReadOnlyCollection<BookViewModel> Books { get; set; }
    }
}
