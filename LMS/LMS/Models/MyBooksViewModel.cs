﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Models
{
    public class MyBooksViewModel
    {
        public CollectionViewModel TakenBooks { get; set; }
        public CollectionViewModel ReservedBooks { get; set; }
    }
}
