﻿namespace LMS.Models
{
    public class NotificationViewModel
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public bool IsRead { get; set; }
        public string CreatedOn { get; set; }
    }
}
