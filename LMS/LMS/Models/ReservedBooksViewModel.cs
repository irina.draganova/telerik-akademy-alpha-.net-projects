﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Models
{
    public class ReservedBooksViewModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public List<string> AuthorsNames { get; set; }
        public string UserId { get; set; }
        public string CurrentUserId { get; set; }
        public int Year { get; set; }
        public string Status { get; set; }
        public double Rating { get; set; }
    }
}
