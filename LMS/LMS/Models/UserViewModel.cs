﻿using LMS.Data.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Models
{
    public class UserViewModel
    {
        public UserViewModel()
        {

        }
        public UserViewModel(string name)
        {
            this.UserName = name ?? throw new ArgumentNullException(nameof(name));

        }
        public string UserId { get; set; } 
        public string UserName { get; set; }
        public string BanId { get; set; }
        public Ban Ban { get; set; }
    }
}
