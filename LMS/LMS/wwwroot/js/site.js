﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.


$(document).ready(function () {
    $('.js-example-basic-multiple').select2();
});

$("#e2").select2();

//const serverResponseHandler = (serverData) => {
//    if (serverData) {
//        $('#put-the-users-here').html(serverData);
//        toastr.success('Search successful');
//    }

//};

//$('#load-button').click(function () {
//    const searchText = $('#usersearch').val();
//    $.get('/Home/Users?name=' + searchText, serverResponseHandler);
//});



/* Set the width of the side navigation to 250px */
function openNav() {
    document.getElementById("mySidenav").style.width = "100%";
}

/* Set the width of the side navigation to 0 */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

$('#get-reviews-button').click(function () {
    const url = $(this).attr('formaction');

    const reviewContainer = $('#reviewtata-tuka');

    reviewContainer.html(`<div class="spinner-border text-success" role="status">
        <span class="sr-only">Loading...</span>
    </div>`);


    $.get(url, function (partialFromTheServerAsAsyncResponse) {
        reviewContainer.html(partialFromTheServerAsAsyncResponse);
    });
});

$("#review").click(function () {
    $.get('/Books/GetJsReviews', serverResponseHandler());
});

const serverResponseHandler = function (serverData) {
    console.log(serverData);
}


