﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Panda.App.Models.Receipt;
using Panda.Data;
using Panda.Domain;

namespace Panda.App.Controllers
{
    public class ReceiptController : Controller
    {
        private readonly PandaDbContext context;

        public ReceiptController(PandaDbContext context)
        {
            this.context = context;
        }
        public IActionResult My()
        {
            List<ReceiptMyViewModel> myReceipts = this.context.Receipts
                .Include(receipt => receipt.Recipient)
                .Where(receipt => receipt.Recipient.UserName == this.User.Identity.Name)
                .Select(receipt => new ReceiptMyViewModel
                {
                    Id = receipt.Id,
                    Fee = receipt.Fee,
                    IssuedOn = receipt.IssuedOn.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                    Recipient = receipt.Recipient.UserName
                })
                .ToList();

            return View(myReceipts);
        }

        [HttpGet("/Receipts/Details/{Id}")]
        public IActionResult Details(string id)
        {
            Receipt receiptFromDb = this.context.Receipts
                .Where(receipt => receipt.Id == id)
                .Include(receipt => receipt.Package)
                .Include(receipt => receipt.Recipient)
                .SingleOrDefault();

            ReceiptDetaillsViewModel viewModel = new ReceiptDetaillsViewModel
            {
                Id = receiptFromDb.Id,
                Total = receiptFromDb.Fee,
                Recipient = receiptFromDb.Recipient.UserName,
                DeliveryAddress = receiptFromDb.Package.ShippingAddress,
                PackageWeight = receiptFromDb.Package.Weight,
                PackageDescription = receiptFromDb.Package.Description,
                IssuedOn = receiptFromDb.IssuedOn.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)
            };

            return this.View(viewModel);
        }
    }
}